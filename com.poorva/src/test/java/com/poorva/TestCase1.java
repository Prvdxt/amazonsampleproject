package com.poorva;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCase1 extends DriverClass {

	public void getBrowser() {
		DriverClass.launchBrowser();
		DriverClass.getURL();

	}

	@Test
	public void testcase1() {
		getBrowser();

		TestCase1Methods tc1m = new TestCase1Methods(driver);
		String status = tc1m.verifyLogo(driver);
		Assert.assertEquals("Amazon", status);

		tc1m.enterSearchDataAndClick(driver);

		boolean status2 = tc1m.verifyListOfSearchResults(driver);
		Assert.assertEquals(true, status2);

		tc1m.addToCart(driver);
		boolean val = tc1m.verifyCart(driver);
		Assert.assertEquals(true, val);

	}

}
