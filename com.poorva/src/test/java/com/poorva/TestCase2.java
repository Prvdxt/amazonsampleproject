package com.poorva;

import junit.framework.Assert;

public class TestCase2 extends DriverClass {

	@Test
	@parameters({"userName","password"})
	public void testcase2(String userName, String password){
		
		TestCase1 tc1=new TestCase1();
		tc1.getBrowser();
		
		tc1.testcase1();
		
		
		TestCase2Methods tc2m=new TestCase2Methods(driver);
	
        tc2m.clickOnCart(driver);
		tc2m.clickOnProceedButton(driver);
		tc2m.login(driver, userName, password)
		;
		boolean status=tc2m.verifyAddressisPrefilled(driver);
		Assert.assertEquals(true, status);
		
		boolean status2=tc2m.verifyPayOnDelivery(driver);
		Assert.assertEquals(true, status2);
		
		tc2m.clickOnPlaceOrderButton(driver);
	
	}

}
