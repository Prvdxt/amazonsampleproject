package com.poorva;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TestCase1Methods {
	WebDriver driver;

	public TestCase1Methods(WebDriver driver) {
		this.driver = driver;
	}

	CommonMethods cm = new CommonMethods();

	public String verifyLogo(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(ObjectRepository.AMAZONLOGO));
		String logo = element.getText();
		return logo;
	}

	public void enterSearchDataAndClick(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(ObjectRepository.SEARCHBOX));
		element.sendKeys(ObjectRepository.SEARCHTEXT);
		WebElement element2 = driver.findElement(By.xpath(ObjectRepository.SEARCHBUTTON));
		element2.click();

	}

	public boolean verifyListOfSearchResults(WebDriver driver) {
		List<WebElement> elements = driver.findElements(By.xpath(ObjectRepository.SEARCHTEXTRESULT));
		for (WebElement ele : elements) {
			if (ele.isDisplayed()) {
				ele.click();

				return true;

			}
			break;
		}
		return false;

	}

	public void addToCart(WebDriver driver) {

		//WebElement ele = driver.findElement(By.xpath(ObjectRepository.SPECIFICWATCH));
		//cm.explicitWait(driver, ele);
		//ele.click();	
		
		ArrayList tabs=new ArrayList( driver.getWindowHandles());
	
		int size=tabs.size()-1;
		System.out.print(size);
	    driver.switchTo().window((String) (tabs.get(size))); 	
	    cm.implicitWait(driver, 10);
		WebElement ele2 = driver.findElement(By.xpath(ObjectRepository.ADDTOCART));
		//cm.explicitWait(driver, ele2);
		ele2.click();
	}	

	public boolean verifyCart(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(ObjectRepository.CARTCOUNT));
		String c = element.getText();
		if (c.equalsIgnoreCase("1")) {

			return true;
		} else {
			return false;
		}
	}

}
