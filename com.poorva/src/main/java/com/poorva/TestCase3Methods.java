package com.poorva;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class TestCase3Methods {

	WebDriver driver;

	public TestCase3Methods(WebDriver driver) {
		this.driver = driver;
	}

	CommonMethods cm = new CommonMethods();

	public void hoverOverCategoryAndClickOnBooks(WebDriver driver) {
		WebElement ele = driver.findElement(By.xpath(ObjectRepository.SHOPCATEGORY));
		Actions act = new Actions(driver);
		act.moveToElement(ele).perform();

		WebElement books = driver.findElement(By.xpath(ObjectRepository.BOOKSLINK));
		books.click();

	}
}
