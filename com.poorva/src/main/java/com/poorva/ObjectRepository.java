package com.poorva;

public class ObjectRepository {

	public static String URL = "https://amazon.in";

	public static String AMAZONLOGO = "//span[@class='nav-sprite nav-logo-base']";

	public static String SEARCHBOX = "//input[@id='twotabsearchtextbox']";
	public static String SEARCHBUTTON = "//input[@value='Go']";
	public static String SHOPCATEGORY = "//span[@class='nav-line-1'][contains(text(),'Shop by')]";
	public static String BOOKSLINK = "//span[@aria-label='Books']";
	public static String CARTCOUNT = "//span[@id='nav-cart-count'][contains(text(),'1')]";
	public static String PROCEEDBUTTON = "//input[@value='Proceed to checkout']";
	public static String SEARCHTEXT = "fossil q ftw2108 marshal digital multi-colour dial men's watch";
	public static String SEARCHTEXTRESULT = "//h2[contains(text(),'Fossil')]";
	public static String SPECIFICWATCH = "//div[@class='s-item-container']//a";
	public static String ADDTOCART = "//input[@id='add-to-cart-button']";
	public static String SIGNINLINK = "//span[@class='nav-line-1'][contains(text(),'Hello. Sign in')]";
	public static String INPUTEMAIL = "//input[@type='email']";
	public static String INPUTPASSWORD = "//input[@type='email']";
	public static String CONTINUEBUTTON = "//input[@id='continue']";
	public static String SUBMITBUTTON = "//input[@id='signInSubmit']";
	public static String COD = "//span[@id='cashondelivery']";
	public static String ADDRESSCHECK = "//li[@class='displayAddressLI displayAddressFullName']";
	public static String PLACEORDER = "//input[@title='Place your order']";
}
