
package com.poorva;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class DriverClass {

	public static WebDriver driver;

	@BeforeClass
public void setProperty()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\hp\\Desktop\\chromedriver.exe");
	}
	
	
	public static WebDriver launchBrowser() {
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		return driver;

	}

	public static void getURL() {
		driver.get(ObjectRepository.URL);
	}

	@AfterClass
	public void closeBrowser() {
		driver.close();
		driver.quit();
	}
}
