package com.poorva;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TestCase2Methods {
	WebDriver driver;

	public TestCase2Methods(WebDriver driver) {
		this.driver = driver;
	}

	CommonMethods cm = new CommonMethods();

	public void clickOnCart(WebDriver driver) {
		cm.implicitWait(driver, 10);
		driver.findElement(By.xpath(ObjectRepository.CARTCOUNT)).click();

	}

	public void clickOnProceedButton(WebDriver driver) {
		cm.implicitWait(driver, 10);
		driver.findElement(By.xpath(ObjectRepository.PROCEEDBUTTON)).click();

	}

	public void login(WebDriver driver, String userName, String password) {

		driver.findElement(By.xpath(ObjectRepository.INPUTEMAIL)).sendKeys(userName);
		driver.findElement(By.xpath(ObjectRepository.CONTINUEBUTTON)).click();
		driver.findElement(By.xpath(ObjectRepository.INPUTPASSWORD)).sendKeys(password);
		driver.findElement(By.xpath(ObjectRepository.SUBMITBUTTON)).click();

	}

	public boolean verifyAddressisPrefilled(WebDriver driver) {
		WebElement ele = driver.findElement(By.xpath(ObjectRepository.ADDRESSCHECK));
		String add = ele.getText();
		if (add != null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean verifyPayOnDelivery(WebDriver driver) {
		WebElement ele = driver.findElement(By.xpath(ObjectRepository.COD));
		String text = ele.getText();
		if (text.contains("Pay on delivery (Cash/Card)")) {
			return true;
		} else {
			return false;
		}
	}

	public void clickOnPlaceOrderButton(WebDriver driver) {
		cm.implicitWait(driver, 10);
		driver.findElement(By.xpath(ObjectRepository.PLACEORDER)).click();

	}
}
