package com.poorva;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethods {

	public static WebDriverWait wait;
	public void explicitWait(WebDriver driver, WebElement element){
		wait.until(ExpectedConditions.visibilityOf(element));
		
	}
public void implicitWait(WebDriver driver, int seconds){
	driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
}
}
